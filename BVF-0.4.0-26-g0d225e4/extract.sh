#!/bin/bash

if [ -d ./deb/ ] ; then
    rm -rf ./deb/ || true
fi
mkdir ./deb/
cd ./deb/
ar x ../bbb.io-gateware*.deb
cd ../

if [ -d ./data/ ] ; then
    rm -rf ./data/ || true
fi
mkdir ./data/
tar xf deb/data.tar.xz -C ./data/

#Cleanup
rm -rf ./deb/ || true